package com.maxossowski.Bibloteka;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class SearchBookAction {

    private Scanner input;
    private Integer chosenNumber;
    private BookRepository bookRepository;

    public SearchBookAction(BookRepository bookRepository){
        this.input = new Scanner(System.in);
        this.chosenNumber = 0;
        this.bookRepository = bookRepository;
    }

    private void searchListMenu() {
        System.out.println(" ");
        System.out.println("----------------------");
        System.out.println("       Search by:     ");
        System.out.println("----------------------");
        System.out.println("1) Title              ");
        System.out.println("2) Publish year       ");
        System.out.println("3) Author             ");
        System.out.println("4) ISBN               ");
        System.out.println("5) Main menu          ");
        System.out.println("----------------------");
    }

    private void printSearchList(List<Book> searchList) {
        for (Book book : searchList) {
            System.out.println(book);
        }
    }

    public void searchBookAction() { // mozna wstawic do osobnej klasy
        boolean searchListFlag = true;
        searchListMenu();
        while (searchListFlag) {
            String userInput = input.nextLine();
            try {
                chosenNumber = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println("Please enter a number!");
                chosenNumber = null;
            }

            if (chosenNumber != null) {
                String title;
                int publishYear = 0;
                String author;
                String isbn;
                List<Book> searchResult;
                switch (chosenNumber) {
                    case 1:
                        System.out.println("Enter title: ");

                        title = input.nextLine();

                        searchResult = bookRepository.searchByTitle(title);
                        if (!searchResult.isEmpty()) {
                            System.out.println("Following books found: ");
                            printSearchList(searchResult);
                        } else {
                            System.out.println("No book with the title '" + title + "' found!");
                        }
                        searchListFlag = false;
                        break;
                    case 2:
                        publishYear = getPublishYearFromUser();

                        searchResult = bookRepository.searchByPublishYear(publishYear);
                        if (!searchResult.isEmpty()) {
                            System.out.println("Following books found: ");
                            printSearchList(searchResult);
                        } else {
                            System.out.println("No book with the publish year '" + publishYear + "' found!");
                        }
                        searchListFlag = false;
                        break;
                    case 3:
                        System.out.println("Enter author: ");

                        author = input.nextLine();

                        searchResult = bookRepository.searchByAuthor(author);
                        if (!searchResult.isEmpty()) {
                            System.out.println("Following books found: ");
                            printSearchList(searchResult);
                        } else {
                            System.out.println("No book with the author '" + author + "' found!");
                        }
                        searchListFlag = false;
                        break;
                    case 4:
                        System.out.println("Enter ISBN: ");

                        isbn = input.nextLine();
                        ISBN searchIsbn = new ISBN(isbn);
                        Optional<Book> searchedBook = bookRepository.findBook(searchIsbn);
                        if (searchedBook.isPresent()) {
                            System.out.println(searchedBook.get());
                        } else {
                            System.out.println("No book with the ISBN '" + searchIsbn + "' found!");
                        }
                        searchListFlag = false;
                        break;
                    case 5:
                        searchListFlag = false;
                        break;
                    default:
                        System.out.println("Choose valid number!");
                        break;
                }
            }
        }
    }

    private Integer getPublishYearFromUser(){
        int publishYear = 0;
        while (publishYear == 0) {
            System.out.println("Enter Publish year:");
            try {
                publishYear = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                publishYear = 0;
            }
            if (publishYear < 1000 || publishYear > LocalDateTime.now().getYear()) {
                System.out.println("Please enter valid year!");
                publishYear = 0;
            }
        }
        return publishYear;
    }

}
