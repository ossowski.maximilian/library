package com.maxossowski.Bibloteka;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Interface {

    private Scanner input;
    private boolean mainLoopFlag;
    private Integer chosenNumber;
    private BookRepository bookRepository;
    private RentBookRepository rentBookRepository;
    private List<String> lines;
    private File file;

    public Interface() {
        this.input = new Scanner(System.in);
        this.mainLoopFlag = true;
        this.chosenNumber = 0;
        this.bookRepository = new BookRepository();
        this.rentBookRepository = new RentBookRepository(bookRepository);
        this.lines = new ArrayList<>();
        this.file = new File("D:\\Prywatne\\Programator\\Books.txt");
    }

    private void showMainMenu() {
        System.out.println(" ");
        System.out.println("----------------------");
        System.out.println("       Book Rent      ");
        System.out.println("----------------------");
        System.out.println("1) Rent Book          ");
        System.out.println("2) Show all Books     ");
        System.out.println("3) Search for Book    ");
        System.out.println("4) Donate Book        ");
        System.out.println("5) Show Menu          ");
        System.out.println("6) Exit               ");
        System.out.println("----------------------");
    }



    public void startProgram()  {
        // Read data from file
        System.out.println("Reading Books form File ...");
        try {
            lines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
        } catch (IOException e) {
            System.out.println("The input file was not found or wrong file content. Continue without data...");
        }
        for (String line : lines) {
            String[] res = line.split(";");
            if (res.length == 4) {
                Book tempBook = new Book(res[0], Integer.parseInt(res[1]), res[2], new ISBN(res[3]));
                bookRepository.addBook(tempBook);
            }
        }

        showMainMenu();
        while (mainLoopFlag) {
            System.out.println("Enter number: ");
            String userInput = input.nextLine();
            try {
                chosenNumber = Integer.parseInt(userInput);
            } catch (NumberFormatException e) {
                System.out.println("Please enter a number!");
                chosenNumber = null;
            }

            if (chosenNumber == null) {continue;}
                switch (chosenNumber) {
                    case 1:
                        System.out.println("Rent Book:");
                        rentBookOption();
                        continueProgram();
                        break;
                    case 2:
                        System.out.println("Show Books:");
                        List<Book> bookList = bookRepository.getBookList();
                        for (Book book : bookList) {
                            System.out.println(book);
                        }
                        continueProgram();
                        break;
                    case 3:
                        System.out.println("Search for Book:");
                        SearchBookAction searchBook = new SearchBookAction(bookRepository);
                        searchBook.searchBookAction();
                        continueProgram();
                        break;
                    case 4:
                        System.out.println("Donate Book: ");
                        donateBookOption();
                        continueProgram();
                        break;
                    case 5:
                        showMainMenu();
                        break;
                    case 6:
                        endProgram();
                        break;
                    default:
                        System.out.println("Choose valid number!");
                        break;
                }

        }
    }

    private void endProgram() {
        mainLoopFlag = false;
    }

    private void continueProgram() {
        System.out.println(" ");
        System.out.println("Back to main menu... click enter to continue");
        input.nextLine();
        showMainMenu();
    }



    private void rentBookOption(){
        System.out.println("Please enter the ISBN number of the book you want to rent: ");
        String isbnInputTemp = input.nextLine();
        ISBN isbnTemp = new ISBN(isbnInputTemp);
        Optional<Book> bookTemp = bookRepository.findBook(isbnTemp); // nazewnictwo findBook dla Optional
        if(bookTemp.isPresent()) {
            if (rentBookRepository.rentBook(isbnTemp, LocalDate.now(), LocalDate.now().plusMonths(1))) {
                System.out.println("Book '" + bookTemp.get().getTitle() + "' was successful loan");
            } else {
                if(rentBookRepository.getRentBookEndTime(isbnTemp).isPresent()) {
                    System.out.println("Book '" + bookTemp.get().getTitle() + "' is already on loan till : " +
                            rentBookRepository.getRentBookEndTime(isbnTemp).get());
                    System.out.println("Choose another position");
                } else {
                    System.out.println("Book '" + bookTemp.get().getTitle() + "' is already on loan");
                    System.out.println("Choose another position");
                }
            }
        } else {
            System.out.println("No book with the ISBN: '" + isbnTemp + "' exists!");
        }
    }

    private void donateBookOption(){
        System.out.println("Please enter ISBN number: ");
        String isbnInput = input.nextLine();
        ISBN isbn = new ISBN(isbnInput);
        Optional<Book> searchedBook = bookRepository.findBook(isbn);
        if(searchedBook.isPresent()){
            System.out.println("Book with the ISBN: '" + isbnInput + "' already exists!");
            System.out.println("No book was added!");
        } else {
        System.out.println("Please enter title: ");
        String title = input.nextLine();
        Integer publishYear = getPublishYearFromUser();
        System.out.println("Please enter author: ");
        String author = input.nextLine();
        Book tempBook = new Book(title, publishYear, author, isbn);
            bookRepository.addBook(tempBook);
            System.out.println("Book was successful added to repository!");
        }
    }

    private Integer getPublishYearFromUser(){
        int publishYear = 0;
        while (publishYear == 0) {
            System.out.println("Enter Publish year:");
            try {
                publishYear = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                publishYear = 0;
            }
            if (publishYear < 1000 || publishYear > LocalDateTime.now().getYear()) {
                System.out.println("Please enter valid year!");
                publishYear = 0;
            }
        }
        return publishYear;
    }




}
