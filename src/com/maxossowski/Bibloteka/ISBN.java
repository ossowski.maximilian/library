package com.maxossowski.Bibloteka;

import java.util.Objects;

public class ISBN implements Comparable{

    private final String isbn;

    public ISBN(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ISBN isbn1 = (ISBN) o;
        return isbn.equals(isbn1.isbn);
    }

    @Override
    public int compareTo(Object o) {
        if (this == o) return 0;
        ISBN isbn1 = (ISBN) o;
        return isbn.compareTo(isbn1.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn);
    }

    @Override
    public String toString() {
        return isbn;
    }
}
