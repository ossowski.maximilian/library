package com.maxossowski.Bibloteka;

import java.util.Objects;

public class Book implements Comparable<Book>{

    private final String title;
    private final Integer yearOfPublish;
    private final String author;
    private final ISBN isbn;

    public Book(String title, Integer yearOfPublish, String author, ISBN isbn) {
        this.title = title;
        this.yearOfPublish = yearOfPublish;
        this.author = author;
        this.isbn = isbn;
    }

    public String getTitle() { return title; }

    public Integer getYearOfPublish() {
        return yearOfPublish;
    }

    public String getAuthor() {
        return author;
    }

    public ISBN getIsbn() { return isbn; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return isbn.equals(book.isbn);
    }

    @Override
    public int compareTo(Book o) {
        return isbn.compareTo(o.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(isbn);
    }

    @Override
    public String toString() {
        return  title + ",   " +
                + yearOfPublish + ",     "
                + author + ",    "
                + isbn;
    }
}
