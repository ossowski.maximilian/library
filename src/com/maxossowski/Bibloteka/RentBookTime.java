package com.maxossowski.Bibloteka;

import java.time.LocalDate;
import java.util.Objects;

public class RentBookTime { // dodac jeszcze Isbn - przydzielenie do ksiazki

    private LocalDate startRentBookTime;
    private LocalDate endRentBookTime;
    private ISBN isbn;

    public RentBookTime(LocalDate startRentBookTime, LocalDate endRentBookTime, ISBN isbn) {
        this.startRentBookTime = startRentBookTime;
        this.endRentBookTime = endRentBookTime;
        this.isbn = isbn;
    }

    public LocalDate getEndRentBookTime() {
        return endRentBookTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RentBookTime that = (RentBookTime) o;
        return Objects.equals(startRentBookTime, that.startRentBookTime) &&
                Objects.equals(endRentBookTime, that.endRentBookTime) &&
                isbn.equals(that.isbn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startRentBookTime, endRentBookTime, isbn);
    }
}
