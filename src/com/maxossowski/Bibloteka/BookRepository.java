package com.maxossowski.Bibloteka;

import java.util.*;

public class BookRepository {

    private Map<ISBN, Book> bookList;

    public BookRepository() {
        this.bookList = new HashMap<>();
    }

    public Optional<Book> findBook(ISBN isbn){
            return Optional.ofNullable(bookList.get(isbn));
    }

    public void addBook(Book book){
        bookList.put(book.getIsbn(), book);
    }

    public List<Book> getBookList() {
        ArrayList<Book> sortedBookList = new ArrayList<>(bookList.values());
        Collections.sort(sortedBookList);// konwersja Seta do Listy
        return sortedBookList;
    }

    public List<Book> searchByTitle(String title){
        List<Book> resultList = new ArrayList<>();
        for (Book book : bookList.values()) {
            if(book.getTitle().contains(title)){
                resultList.add(book);
            }
        }
        return resultList;
    }

    public List<Book> searchByPublishYear(Integer publishYear){
        List<Book> resultList = new ArrayList<>();
        for (Book book : bookList.values()) {
            if(book.getYearOfPublish().equals(publishYear)){
                resultList.add(book);
            }
        }
        return resultList;
    }

    public List<Book> searchByAuthor(String author){
        List<Book> resultList = new ArrayList<>();
        for (Book book : bookList.values()) {
            if(book.getAuthor().contains(author)){
                resultList.add(book);
            }
        }
        return resultList;
    }





}
