package com.maxossowski.Bibloteka;

import java.time.LocalDate;
import java.util.*;

public class RentBookRepository {

    private Map<ISBN, List<RentBookTime>> loanBooks;
    private BookRepository bookRepository;

    public RentBookRepository(BookRepository bookRepository) {
        this.loanBooks = new HashMap<>();
        this.bookRepository = bookRepository;
    }

    public boolean rentBook(ISBN isbn, LocalDate startDate, LocalDate endDate) {
        if (checkIfBookIsAvailable(isbn)) {
            List<RentBookTime> rentBookTimes = loanBooks.get(isbn);
            for (RentBookTime rentTime : rentBookTimes) {
                if (rentTime.getEndRentBookTime().isAfter(startDate)) {
                    return false;
                }
            }
            RentBookTime newRentEntryTime = new RentBookTime(startDate, endDate, isbn);
            rentBookTimes.add(newRentEntryTime);
            return true;
        } else {
            List<RentBookTime> rentBookTimes = new ArrayList<>();
            RentBookTime newRentEntryTime = new RentBookTime(startDate, endDate, isbn);
            rentBookTimes.add(newRentEntryTime);
            loanBooks.put(isbn, rentBookTimes);
            return true;
        }

    }

    private boolean checkIfBookIsAvailable(ISBN isbn) {
        Optional<Book> bookCheck = bookRepository.findBook(isbn);
        if (bookCheck.isPresent()) {
            return loanBooks.containsKey(isbn);
        }
        return false;
    }

    public Optional<LocalDate> getRentBookEndTime(ISBN isbn) {
        if (checkIfBookIsAvailable(isbn)) {
            List<RentBookTime> rentBookTimes = loanBooks.get(isbn);
            for (RentBookTime rentTime : rentBookTimes) {
                if (rentTime.getEndRentBookTime().isAfter(LocalDate.now())) {
                    return Optional.of(rentTime.getEndRentBookTime());
                }
            }
        }
        return Optional.empty();
    }


}
