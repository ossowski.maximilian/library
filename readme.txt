Book Library

Specification:

Write a program for a public library with the following functionality:

- The program should run until the user doesn't wirte "exit", every functionalty should get into a submenu and offer the possibility to switch between those.
- Book catalogs for rent (the books should be stored in a file)
    - The book should have at least: title, publishing year, author and its ID (for example ISBN)
- The data about the rent books should be stored in the memory, create a class that should represent the book repository
    - Only this class should have the option to operate on the books, (rent them etc.)
- Through the UI the user should be able to rent books and if its not available he/she should get an appropriate message
- The user should be able to view a list of all books available
- The user should be able to search for books by all parameters (title phrase, publishing year etc.)
- The user should be able to add his own book to the collection (donation)
    - No duplicates should be accepted (check by ISBN)

Architecture:
- basic console Application

Technologies/Tools:
- OOP
